import React from 'react';
import './TaskForm.css';

const AddTaskForm = props => {
  return (
    <div className="TaskForm">
      <p><input type="text" placeholder="Add new task" value={props.value} onChange={props.change} /></p>
      <button type="button" onClick={props.add}>Add</button>
    </div>
  );
};

export default AddTaskForm;