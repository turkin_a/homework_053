import React from 'react';
import './Task.css';
import './fonts/font-awesome.css';

const Task = props => {
  return (
    <div className={props.classList}>
      <p className="Task">{props.info}</p>
      <i className="icon fa fa-trash-o" aria-hidden="true" onClick={props.del}> </i>
      <span className="icon check-square"><i className="fa fa-check" aria-hidden="true" onClick={props.resolved}> </i></span>
    </div>
  );
};

export default Task;