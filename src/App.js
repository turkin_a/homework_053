import React, { Component } from 'react';
import './App.css';
import AddTaskForm from "./TaskForm";
import Task from "./Task";

class App extends Component {
  state = {
    tasks: [
      {info: 'Buy milk', id: 't1', resolved: false},
      {info: 'Walk with dog', id: 't2', resolved: false},
      {info: 'Do homework', id: 't3', resolved: false}
    ],
    currentTask: ''
  };

  currentTask = (event) => {
    const currentTask = event.target.value;

    this.setState({currentTask});
  };

  addTask = () => {
    const tasks = [...this.state.tasks];

    if (this.state.currentTask) {
      tasks.push({info: this.state.currentTask, id: Date.now(), resolved: false});

      this.setState({tasks, currentTask: ''});
    }
  };

  deleteTask = (event, id) => {
    const tasks = [...this.state.tasks];
    const index = this.state.tasks.findIndex(t => t.id === id);

    tasks.splice(index, 1);

    this.setState({tasks});
  };

  markAtResolved = (event, id) => {
    const tasks = [...this.state.tasks];
    const index = this.state.tasks.findIndex(t => t.id === id);

    tasks[index].resolved = !tasks[index].resolved;

    this.setState({tasks});
  };

  render() {
    let tasks = (
      <div>
        {
          this.state.tasks.map((task) => {
            return <Task
              classList={'TaskBox' + (task.resolved ? ' resolved' : '')}
              key={task.id}
              info={task.info}
              del={(event) => this.deleteTask(event, task.id)}
              resolved={(event) => this.markAtResolved(event, task.id)}
            />
          })
        }
      </div>
    );

    return (
      <div className="App">
        <AddTaskForm
          change={(event) => this.currentTask(event)}
          add={this.addTask}
          value={this.state.currentTask}
        />
        <div className="Tasks">
          {tasks}
        </div>
      </div>
    );
  }
}

export default App;
